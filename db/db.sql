
CREATE TABLE movie(movie_id INTEGER primary key auto_increment,
                    movie_title VARCHAR(50),
                     movie_release_date date,
                     movie_time VARCHAR(30),
                     director_name VARCHAR(50)
);

INSERT INTO movie(movie_title,movie_release_date,movie_time,director_name)
VALUES('matrix','2022-01-02','12:00','noland');