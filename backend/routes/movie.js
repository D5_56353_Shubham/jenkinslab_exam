const express = require("express");
const db = require("../db");

const utils = require("../utils");
const router = express.Router();

//display movies
router.get("/", (request, response) => {
  const statement = `select * from movie`;
  db.execute(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

// 2. POST --> ADD Movie data into Containerized MySQL table

// 3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

// 4. DELETE --> Delete Movie from Containerized MySQL

//Add movie
router.post("/add_movie", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;
  const statement = `insert into movie (movie_title,movie_release_date,movie_time,director_name)
    values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`;
  db.execute(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});
//update movie
router.put("/update_movie/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const { movie_release_date, movie_time } = request.body;
  const statement = `update movie set
  movie_release_date='${movie_release_date}'
  movie_time ='${movie_time}'
  where movie_id=${movie_id}`;
  db.execute(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

//delete movie
router.delete("/delete_movie/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const statement = `delete from movie
  where movie_id=${movie_id}`;
  db.execute(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const statement = `select * from movie
  where movie_id=${movie_id}`;
  db.execute(statement, (error, result) => {
    if (result.length > 0) {
      response.send(utils.createResult(error, result[0]));
    } else {
      response.send(utils.createResult("movie not exit"));
    }
  });
});
module.exports = router;
