const express = require("express");
const routerMovie = require("./routes/movie");
const cors = require("cors");
const app = express();

app.use(cors("*"));
app.use(express.json());
app.use("/movie", routerMovie);

app.listen(4000, () => {
  console.log(`Server started on port 4000`);
});
